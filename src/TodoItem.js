import { ACTIONS } from './App'

const TodoItem = ({ dispatch, todo }) => (
  <li className='todo-item'>
    <input
      checked={todo.complete}
      className='check'
      onChange={() =>
        dispatch({
          type: ACTIONS.TOGGLE_TODO,
          payload: { name: todo.name }
        })}
      type='checkbox'
    />

    <span
      className='text'
      style={{ color: todo.complete ? '#aaa' : '#000' }}
    >
      {todo.name}
    </span>

    <button
      className='delete'
      onClick={() =>
        dispatch({
          type: ACTIONS.DELETE_TODO,
          payload: { name: todo.name }
        })}
    >
      ⨯
    </button>
  </li>
)

export default TodoItem
