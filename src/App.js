import { useReducer, useState } from 'react'

import './App.css'
import TodoItem from './TodoItem'

// Constants for reducer
export const ACTIONS = {
  ADD_TODO: 'add-todo',
  DELETE_TODO: 'delete-todo',
  TOGGLE_TODO: 'toggle-todo'
}

const PROFILE_LINK = 'https://iansmcb.ml/'
const LICENSE_LINK = 'https://gitlab.com/ian-s-mcb/react-reducer-todo-list/-/blob/main/LICENSE'

function App () {
  const [todos, dispatch] = useReducer(reducer, [])
  const [todoInput, setTodoInput] = useState('')

  function handleSubmit (e) {
    e.preventDefault()
    if (!todoInput) return
    dispatch({ type: ACTIONS.ADD_TODO, payload: { name: todoInput } })
    setTodoInput('')
  }

  return (
    <>
      <h1>Todo List</h1>
      <form id='todoForm' onSubmit={handleSubmit}>
        <input
          id='todoInput'
          onChange={e => setTodoInput(e.target.value)}
          placeholder='Enter a new todo'
          type='text'
          value={todoInput}
        />
        <button type='submit'>Create</button>
      </form>
      <hr />
      <ul>
        {todos.length
          ? todos.map(todo =>
            <TodoItem
              dispatch={dispatch}
              key={todo.name}
              todo={todo}
            />)
          : <span>No todo's created</span>}
      </ul>
      <footer>
        Copyright @ 2021{' '}
        <a href={PROFILE_LINK} >
          Ian S. McBride
        </a>.
        This work is licensed under the{' '}
        <a href={LICENSE_LINK}>MIT License</a>.
      </footer>
    </>
  )
}

// Helper for reducer
const newTodo = name => ({
  complete: false,
  tsCreated: Date.now(),
  name
})

function reducer (todos, action) {
  switch (action.type) {
    case ACTIONS.ADD_TODO:
      return [...todos, newTodo(action.payload.name)]
    case ACTIONS.DELETE_TODO:
      return todos.filter(todo => todo.name !== action.payload.name)
    case ACTIONS.TOGGLE_TODO:
      return todos.map(
        todo => {
          if (todo.name === action.payload.name) {
            return { ...todo, complete: !todo.complete }
          }
          return todo
        }
      )
    default:
      return todos
  }
}

export default App
