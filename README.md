# react-reducer-todo-list

A useReducer-based todo list frontend app built with React. No backend
included, so todos do not persist.

### [Demo][demo]

### Screencast

![short screencast of project][screencast]

[demo]: https://react-reducer-todo-list.netlify.app/
[screencast]: https://i.imgur.com/mWzx4Mw.gif
